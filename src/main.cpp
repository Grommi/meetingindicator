#include <Arduino.h>
#include <Credentials.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <ESP8266mDNS.h> 

/*
 * based on 
 * uMQTTBroker demo for Arduino (C++-style)
 * and added mDNS and ElegantOTA for convenience
 * red LEDs when you're in a meeting, green LEDs when not 
 */

#include <ESP8266WiFi.h>
#include "uMQTTBroker.h"

#include <FastLED.h>

#define NUM_LEDS 3
#define DATA_PIN D5

CRGB leds[NUM_LEDS];

bool WiFiAP = false;      // Do yo want the ESP as AP?
String status = "No client connected";

// static ip
IPAddress local_IP(192, 168, 0, 130);
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 0, 0);

AsyncWebServer server(80);

void all_leds(CRGB color){
  for(int i=0; i<NUM_LEDS; i++){
    leds[i] = color;
  }
}

/*
 * Custom broker class with overwritten callback functions
 */
class myMQTTBroker: public uMQTTBroker
{
public:
    virtual bool onConnect(IPAddress addr, uint16_t client_count) {
      Serial.println(addr.toString()+" connected");
      return true;
    }

    virtual void onDisconnect(IPAddress addr, String client_id) {
      Serial.println(addr.toString()+" ("+client_id+") disconnected");
    }

    virtual bool onAuth(String username, String password, String client_id) {
      Serial.println("Username/Password/ClientId: "+username+"/"+password+"/"+client_id);
      return true;
    }
    
    virtual void onData(String topic, const char *data, uint32_t length) {
      char data_str[length+1];
      os_memcpy(data_str, data, length);
      data_str[length] = '\0';
      
      if (topic == "meeting"){
        if (data[0] == '0'){
          status = "No meeting";
          all_leds(CRGB::Green);
        }
        if (data[0] == '1'){
          status = "Meeting running";
          all_leds(CRGB::Red);
        }
        Serial.println("Meeting running");
        FastLED.show();
      }
      else{
        Serial.println("received topic '"+topic+"' with data '"+(String)data_str+"'");
      }
    }

    virtual void printClients() {
      for (int i = 0; i < getClientCount(); i++) {
        IPAddress addr;
        String client_id;
         
        getClientAddr(i, addr);
        getClientId(i, client_id);
        Serial.println("Client "+client_id+" on addr: "+addr.toString());
      }
    }
};

myMQTTBroker myBroker;

/*
 * WiFi init stuff
 */
void startWiFiClient()
{
  Serial.println("Connecting to "+(String)WIFI_SSID);
  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet))
  {
    Serial.println("STA Failed to configure");
  }
  WiFi.begin(WIFI_SSID, WIFI_PW);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  
  Serial.println("WiFi connected");
  Serial.println("IP address: " + WiFi.localIP().toString());
}

void startWiFiAP()
{
  WiFi.mode(WIFI_AP);
  WiFi.softAP(WIFI_SSID, WIFI_PW);
  Serial.println("AP started");
  Serial.println("IP address: " + WiFi.softAPIP().toString());
}

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  // Start WiFi
  if (WiFiAP)
    startWiFiAP();
  else
    startWiFiClient();

  // Start the broker
  Serial.println("Starting MQTT broker");
  myBroker.init();
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", status);
  });

  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");

/*
 * Subscribe to anything
 */
  myBroker.subscribe("#");
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(127);
  all_leds(CRGB::Black);
  FastLED.show();

  
  if (!MDNS.begin("meeting")) {             // Start the mDNS responder for esp8266.local
    Serial.println("Error setting up MDNS responder!");
  }
  Serial.println("mDNS responder started. You might be able to reach the device with http://meeting/");
  
  MDNS.addService("http", "tcp", 80);
}

int counter = 0;

void loop()
{
  if (myBroker.getClientCount() > 0){
    myBroker.printClients();
  }
  else{
    all_leds(CRGB::Black);
    FastLED.show();
  }
  AsyncElegantOTA.loop();
  MDNS.update();
  delay(10000);
}