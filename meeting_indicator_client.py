import sys
import subprocess
import paho.mqtt.client as mqtt
import time

print("Starting MeetingIndicator")
# Please insert the IP of your ESP and rename client
mqtt_broker = "192.168.0.130"
client_name = "mqtt_client"

apps = [
    {
        'exe': "CptHost.exe",
    },
    {
        'exe': "Teams.exe",
        'keywords': ["Bespr", "Meet"],
    },
]


def start_conn():
    """
    Connect to the MQTT broker
    :return:
    """
    print("Connecting to " + mqtt_broker + " .", end="")
    while not client.connected_flag:
        try:
            if client.connect(mqtt_broker) == 0:
                client.connected_flag = True
                print(" Connected")
        except TimeoutError:
            print(".", end="")
        except ValueError:
            print("\nInvalid host. Did you insert the IP of your ESP?")
            sys.exit()


def meeting_running():
    for app in apps:
        output = str(subprocess.check_output(f'tasklist /fo table /v /fi "imagename eq {app["exe"]}" /nh'))
        # remove "/nh" to get a header in the table
        # p = os.popen('tasklist /u /fo table /v /fi "imagename eq CptHost.exe" && tasklist /fo table /v /fi "imagename eq Teams.exe" /fi "windowtitle eq Meet*" /nh')
        for keyword in app.get('keywords', [app['exe']]):
            if output.find(keyword) != -1:
                return True
    return False


if __name__ == "__main__":
    mqtt.Client.connected_flag = False  # create flag in class
    client = mqtt.Client(client_name)
    start_conn()

    meeting = not meeting_running()
    meeting_start = time.time()

    while True:
        rc = 0
        if meeting_running():
            if not meeting:
                meeting = True
                meeting_start = time.time()
                print("Meeting started")
            rc, mid = client.publish("meeting", 1)
        else:
            if meeting:
                meeting = False
                print(f"Meeting ended. Duration: {round((time.time() - meeting_start) / 60)} min")
            rc, mid = client.publish("meeting", 0)
        if rc != 0:
            print("Lost connection. Trying to reconnect")
            client.connected_flag = False
            meeting = False
            start_conn()
        time.sleep(1)
