# MeetingIndicator

This app detects if you're having a meeting in Zoom or Teams by checking your task list and notifying
a ESP8266 which works as a MQTT broker which will control a WS2812 LED strip. The LEDs indicate
when you're in a meeting or not. Currently working on windows only but porting it to Linux or Mac
should be easy. 
Inspired by [AIIM](https://github.com/naztronaut/AIIM) which seemed a bit overkill for this task 
 with a Pi and based on 
[uMQTTBroker example](https://github.com/martin-ger/uMQTTBroker/tree/master/examples/uMQTTBrokerSampleOOFull).
I added [mDNS](https://techtutorialsx.com/2016/11/20/esp8266-webserver-resolving-an-address-with-mdns/)
 and [OTA (Over the Air) updates](https://randomnerdtutorials.com/esp32-ota-over-the-air-vs-code/) 
 for convenience. You should be able to reach the ESP and get the 
meeting status with http://meeting/ and update with http://meeting/update or with the IP

## Installation

1. Clone repo
2. pip install -r requirements.txt
3. Change IP and client name in `meeting_indicator_client.py` 
4. Copy `src/Credentials_example.h` to `src/Credentials.h` and change SSID and password in the file.
5. Install platformio dependencies listed in `platform.ini`.
6. Connect WS2812 LEDs data to pin `D5` and power supply.
7. Flash your ESP8266 in platformio.
8. Run `python meeting_indicator_client.py` on your meeting computer.
9. You can use Scheduled Task in Windows to automatically start the script.

## Case
I 3d printed [this case](www.thingiverse.com/thing:2672966) and glued the LEDs to it.

![](img/green_n_red.jpg)

